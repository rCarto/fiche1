---
abstract: Super abstract.
authors:
- admin
- user1
- user2
date: "2013-07-01T00:00:00+02:00"
doi: ""
featured: true
image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
  focal_point: ""
links:
- name: Custom Link
  url: http://example.org
projects:
- internal-project
publication: In *Source Themes Conference*
publication_short: In *STC*
publication_types:
- "1"
slides: example
summary: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere tellus
  ac convallis placerat. Proin tincidunt magna sed ex sollicitudin condimentum.
tags:
- Source Themes
title: Notre example
url_code: '#'
url_dataset: '#'
url_pdf: http://eprints.soton.ac.uk/352095/1/Cushen-IMV2013.pdf
url_poster: '#'
url_project: ""
url_slides: ""
url_source: '#'
url_video: '#'
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## R Markdown


```{r cars}
library(sf)
library(cartography)
par(mar = c(0,0,0,0))
com <- st_read("data/layers58.gpkg", layer="com")
typoLayer(x = com, var="NOM_DEP") 
```

## Including Plots




![](img/nevers.png)


```{r}
knitr::include_graphics("img/nevers.png")
```
